# Yin Yu's README



## Yin Yu - Director of Strategy and Operations

This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. It’s also a well-intentioned effort at building trust by being intentionally vulnerable, and to share my ideas of a good working relationship.


## About Me

1.  My current role is **Director of Strategy and Operations**, in [Office of the CEO](https://handbook.gitlab.com/handbook/ceo/office-of-the-ceo/), with a focus on [JiHu](https://about.gitlab.com/pricing/faq-jihu/), GitLab’s investment in China through the independently run joint venture company.
3. I am also the co-lead of [GitLab's Mental Health TMAG](https://handbook.gitlab.com/handbook/company/culture/inclusion/tmag-mental-health/) (Team Member Advocacy Group), and part of GitLab's [Leadership Diversity Inclusion & Belonging Council](https://handbook.gitlab.com/handbook/company/culture/inclusion/leadership-dib-council/), to advance the endeavors that are close to my heart, professionally and personally. 
2. I am based out of [Sydney](https://en.wikipedia.org/wiki/Sydney), Australia. I was born and spent the first 20+ years in China’s Yangtze Delta area ([Shanghai](https://en.wikipedia.org/wiki/Shanghai) & [Ningbo](https://en.wikipedia.org/wiki/Ningbo) to be specific), then moved back and forth between France ([Grenoble](https://en.wikipedia.org/wiki/Grenoble), in the French Alps) and China, before calling Sydney home since 2008. 
3. Before GitLab, I worked in a giant org (Microsoft), an one-woman enterprise (my own consultancy) and a few with varying sizes somewhere in between, wearing different hats  (business consultant, project/program management, BD, sales, account management, customer success, readiness and whatever hats needed to pull a business off the ground). What connects these experiences is the focus on **global expansion** esp into APAC (sometimes with a specific focus on ANZ, or China) and on **managing strategic partnerships/relationships**. It makes me pretty good at **connecting dots**, **switching contexts** (cross region/language/functions), and **solving complex problems** while being in tune with a **balanced view**. 
4. I have a Master's degree in International Business from Ecole de Commerce in Grenoble/France, and a Bachelor of Art in English Language and Literature from Fudan University/Shanghai. 
5. I speak **English, Mandarin, French**, and two Chinese dialets, including Ningbo dialet, which was the only language I spoke for the first few years of my life before starting school. 

## My Working Style

1. I value **trust**. For me it’s the true north of all meaningful professional (and personal) relationships. The [Trust Equation](https://modelthinkers.com/mental-model/trust-equation) states that trustworthiness is equal to the sum of credibility, reliability and intimacy divided by a person’s self-orientation. This guides how I expect of myself in my professional (and personal) endeavors. 
2. Over the years, I have been practising and getting better at **saying no**. It has not been easy - esp. for someone who grew up in a culture valuing harmony and collectivism - but I’m getting better. Please call me out if you think I should have said no but didn’t. 
3. I am **curious** and **love learning** - this has led me to many wonderful opportunities (including this role by responding to a somewhat curious message on LinkedIn) but at times distracted me. I now use 80/20 rules - 80% of my energy on the defined priorities and allow 20% of my energy on keeping on discovering. 
4. I am a **self starter**, and I am **organized**. This means I typically am on top of my to-do/slack/emails and I (re)prioritize regularly.
5. I take **commitment** seriously - I prefer under-promise and over-deliver than the other way around. I expect myself - and others - to do what one says, or proactively communicate when one cannot. 
6. I have a natural tendancy of **embracing all sides of an argument**, which I think is important for being inclusive, balanced, and empathetic, but it could also come across as 'opinion not held strong enough'. 

## Work With Me

1. I like to **understand the context** - the ‘big picture’. I get frustrated when I don’t know the context when asked to do something. I seek to understand so that I know the value of the tasks, and how best I can contribute - there may be better ways of achieving the same goal, but only if/when I know what the goal is. 
2. I welcome **feedback** - both raving praises and constructive feedback. I prefer clarity when it comes to feedback. I try to do the same with others. If you ever feel like I could be doing something differently to be more helpful to you, your team, or the company, please let me know.
3. **Deep connection and 1:1** are my favorite forms of communication, although I am perfectly comfortable and have a track record of giving talks to audience of various sizes (from community clubs to international conferences)
4. As I work mostly with folks from different time zones other than Sydney (GMT+11), I **adjust my days to be as inclusive and productive as possible**, while respecting my parenting duties. For example, two days a week (at the moment on Thursdays and Fridays) I start at 7am to maximize the overlap with US. Accordingly, I adjust my typical finish hours on these days, and make further adjument on the days when I take evening meetings (esp. with China). My calendar setting reflects these adjustments. 
5. I love coffee chats. This year one of my development goals is to **enhance my understanding of different functions** within Gitlab, esp the ones that I don't get to work with normally. Please feel free to put something into my calendar - preferably at least two days in advance. I also reach out to others for coffee chats and appreciate your time. 
6. I welcome connection on [**LinkedIn**](https://www.linkedin.com/in/callmeyin/) - I accept connection requests from people I have worked with or have a genuine reason for connection if we have not worked together. 

## Personality Tests

1. [**Gallup CliftonStrengths**](https://www.gallup.com/cliftonstrengths/en/253715/34-cliftonstrengths-themes.aspx) (2022)

    **'Strategic Thinking'** is my leading theme, and my top 5 strengths are:
    1. **Ideation**: 
_‘Fascinated by ideas; able to find connections between seemingly disparate phenomena’_
    2. **Connectedness**: 
_‘Have faith in the links among all things’_
    3. **Learner**:
_‘Have a great desire to learn and want to continuously improve; the process of learning, rather than the outcomes, excites them.’_
    4. **Relator**:
_‘Enjoy close relationships with others; they find deep satisfaction in working hard with friends to achieve a goal’_
    5. **Strategic**: 
_‘Create alternative ways to proceed; faced with any given scenario, they can quickly spot the relevant patterns and issues’_

2. **16 Personalities** 

    According to a 2023 test: 
    - Personality type: Protagonist [**ENFJ-A**](https://www.16personalities.com/enfj-personality) 
    - Traits: Extraverted – 52%, Intuitive – 63%, Feeling – 52%, Judging – 60%, Assertive – 78%
    - Role: Diplomat
    - Strategy: People Mastery
    
    However I used to be an [ENTP](https://www.16personalities.com/entp-personality) - an 'Analyst-Debator' personality type - when tested about a decade ago. 

## Me away from GitLab
1. I **co-parent** a **near-teen trilingual neuro-divergent** [**third-culture**](https://en.wikipedia.org/wiki/Third_culture_kid) daughter. It’s fun, consuming, challenging, humbling, eye-opening, stimulating and fulfilling all at once. Massive privilege and huge responsibility. Lots of ups, and lots of downs, on repeat. Endless learning. 
2. I love being in **nature**, and try my hands on different **outdoor activities**
    - sailing: currently I'm training to race in an [Adams-10 keelboat](https://www.mysailing.com.au/adams-10-one-design-racing-the-best-of-both-worlds/) (competed in Australia’s [Adams-10 National Championship 2022](https://www.youtube.com/watch?v=0Jzi6TpAa7g)/2023)
    - bushwalking/hiking: enjoy long hikes in different continents (such as a recent 4-day [Milford Track](https://www.doc.govt.nz/parks-and-recreation/places-to-go/fiordland/places/fiordland-national-park/things-to-do/tracks/milford-track/) in New Zealand), as much as a neighborhood stroll (I am lucky to live next to two national parks and one of the most beautiful harbors in the world)
    - open water swimming
    - a bit of social cycling ([Great Cycle Challenge](https://greatcyclechallenge.com.au/riders/YinYu) every Oct for the last 4 years) 
    - once an accidental Champion of [NSW Rogaining](https://nswrogaining.org/) State Championship (ask me about it in our coffee chat if you want to know more). 
    - aiming to complete one Olympic distance Triathlon before 2028 (first task: overcome my dislike of running). 
3. I also enjoy a traditional Chinese tea brewing ceremony (my favorites are Oolong and Pu'er at the moment), writing my first book (four pillars for a quality life), learning/practicing building financial freedom (advocating for female financial education), having a deep conversation with my friends, listening to podcasts of various subjects (recommendations welcome), and experiencing new experiences/places/cultures (once traveled with my 2-year-old around the world for 10 months).  


## Thanks for Reading
As you get to know me, please share when you see a pattern of how I work that you think would be beneficial to articulate in this document.

If you have any questions, please reach out to me or schedule a coffee chat!